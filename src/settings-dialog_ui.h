/* automatically generated from settings-dialog.ui */
#ifdef __SUNPRO_C
#pragma align 4 (settings_dialog_ui)
#endif
#ifdef __GNUC__
static const char settings_dialog_ui[] __attribute__ ((__aligned__ (4))) =
#else
static const char settings_dialog_ui[] =
#endif
{
  "<?xml version=\"1.0\" encoding=\"UTF-8\"?><interface><requires lib=\"gt"
  "k+\" version=\"3.22\"/><requires lib=\"libxfce4ui-2\" version=\"4.12\"/"
  "><object class=\"GtkImage\" id=\"image1\"><property name=\"visible\">Tr"
  "ue</property><property name=\"can_focus\">False</property><property nam"
  "e=\"icon_name\">help-browser</property></object><object class=\"GtkImag"
  "e\" id=\"image2\"><property name=\"visible\">True</property><property n"
  "ame=\"can_focus\">False</property><property name=\"icon_name\">help-abo"
  "ut</property></object><object class=\"GtkImage\" id=\"image3\"><propert"
  "y name=\"visible\">True</property><property name=\"can_focus\">False</p"
  "roperty><property name=\"icon_name\">window-close-symbolic</property></"
  "object><object class=\"GtkListStore\" id=\"liststore1\"><columns><colum"
  "n type=\"gint\"/></columns><data><row><col id=\"0\">500</col></row><row"
  "><col id=\"0\">750</col></row><row><col id=\"0\">1000</col></row><row><"
  "col id=\"0\">2000</col></row><row><col id=\"0\">5000</col></row><row><c"
  "ol id=\"0\">10000</col></row></data></object><object class=\"XfceTitled"
  "Dialog\" id=\"settings-dialog\"><property name=\"can_focus\">False</pro"
  "perty><property name=\"title\" translatable=\"yes\">Task Manager Settin"
  "gs</property><property name=\"icon_name\">org.xfce.taskmanager</propert"
  "y><property name=\"type_hint\">normal</property><child internal-child=\""
  "vbox\"><object class=\"GtkBox\"><property name=\"can_focus\">False</pro"
  "perty><property name=\"orientation\">vertical</property><property name="
  "\"spacing\">2</property><child internal-child=\"action_area\"><object c"
  "lass=\"GtkButtonBox\" id=\"dialog-action_area1\"><property name=\"can_f"
  "ocus\">False</property><property name=\"layout_style\">end</property><c"
  "hild><object class=\"GtkButton\" id=\"button-help\"><property name=\"la"
  "bel\" translatable=\"yes\">_Help</property><property name=\"visible\">T"
  "rue</property><property name=\"can_focus\">True</property><property nam"
  "e=\"receives_default\">True</property><property name=\"image\">image1</"
  "property><property name=\"use_underline\">True</property></object><pack"
  "ing><property name=\"expand\">True</property><property name=\"fill\">Tr"
  "ue</property><property name=\"position\">0</property></packing></child>"
  "<child><object class=\"GtkButton\" id=\"button-about\"><property name=\""
  "label\" translatable=\"yes\">About</property><property name=\"visible\""
  ">True</property><property name=\"can_focus\">True</property><property n"
  "ame=\"receives_default\">True</property><property name=\"image\">image2"
  "</property></object><packing><property name=\"expand\">True</property><"
  "property name=\"fill\">True</property><property name=\"position\">1</pr"
  "operty></packing></child><child><object class=\"GtkButton\" id=\"button"
  "-close\"><property name=\"label\" translatable=\"yes\">_Close</property"
  "><property name=\"visible\">True</property><property name=\"can_focus\""
  ">True</property><property name=\"receives_default\">True</property><pro"
  "perty name=\"image\">image3</property><property name=\"use_underline\">"
  "True</property></object><packing><property name=\"expand\">False</prope"
  "rty><property name=\"fill\">False</property><property name=\"position\""
  ">2</property></packing></child></object><packing><property name=\"expan"
  "d\">False</property><property name=\"fill\">False</property><property n"
  "ame=\"position\">0</property></packing></child><child><object class=\"G"
  "tkNotebook\"><property name=\"visible\">True</property><property name=\""
  "can_focus\">True</property><child><object class=\"GtkBox\" id=\"vbox1\""
  "><property name=\"visible\">True</property><property name=\"can_focus\""
  ">False</property><property name=\"border_width\">12</property><property"
  " name=\"orientation\">vertical</property><property name=\"spacing\">12<"
  "/property><child><object class=\"GtkFrame\" id=\"frame-interface\"><pro"
  "perty name=\"visible\">True</property><property name=\"can_focus\">Fals"
  "e</property><property name=\"label_xalign\">0</property><property name="
  "\"shadow_type\">none</property><child><object class=\"GtkBox\" id=\"vbo"
  "x-interface\"><property name=\"visible\">True</property><property name="
  "\"can_focus\">False</property><property name=\"margin_left\">12</proper"
  "ty><property name=\"margin_top\">6</property><property name=\"orientati"
  "on\">vertical</property><property name=\"spacing\">6</property><child><"
  "object class=\"GtkCheckButton\" id=\"button-show-all-processes\"><prope"
  "rty name=\"label\" translatable=\"yes\">Show all processes</property><p"
  "roperty name=\"visible\">True</property><property name=\"can_focus\">Tr"
  "ue</property><property name=\"receives_default\">False</property><prope"
  "rty name=\"draw_indicator\">True</property></object><packing><property "
  "name=\"expand\">False</property><property name=\"fill\">True</property>"
  "<property name=\"position\">0</property></packing></child><child><objec"
  "t class=\"GtkCheckButton\" id=\"button-show-application-icons\"><proper"
  "ty name=\"label\" translatable=\"yes\">Show application icons</property"
  "><property name=\"visible\">True</property><property name=\"can_focus\""
  ">True</property><property name=\"receives_default\">False</property><pr"
  "operty name=\"draw_indicator\">True</property></object><packing><proper"
  "ty name=\"expand\">False</property><property name=\"fill\">True</proper"
  "ty><property name=\"position\">1</property></packing></child><child><ob"
  "ject class=\"GtkCheckButton\" id=\"button-full-command-line\"><property"
  " name=\"label\" translatable=\"yes\">Show full command lines</property>"
  "<property name=\"visible\">True</property><property name=\"can_focus\">"
  "True</property><property name=\"receives_default\">False</property><pro"
  "perty name=\"draw_indicator\">True</property></object><packing><propert"
  "y name=\"expand\">False</property><property name=\"fill\">False</proper"
  "ty><property name=\"position\">2</property></packing></child><child><ob"
  "ject class=\"GtkCheckButton\" id=\"button-process-tree\"><property name"
  "=\"label\" translatable=\"yes\">Show processes as tree</property><prope"
  "rty name=\"visible\">True</property><property name=\"can_focus\">True</"
  "property><property name=\"receives_default\">False</property><property "
  "name=\"draw_indicator\">True</property></object><packing><property name"
  "=\"expand\">False</property><property name=\"fill\">False</property><pr"
  "operty name=\"position\">3</property></packing></child><child><object c"
  "lass=\"GtkCheckButton\" id=\"button-show-legend\"><property name=\"labe"
  "l\" translatable=\"yes\">Show legend</property><property name=\"visible"
  "\">True</property><property name=\"can_focus\">True</property><property"
  " name=\"receives_default\">False</property><property name=\"draw_indica"
  "tor\">True</property></object><packing><property name=\"expand\">False<"
  "/property><property name=\"fill\">True</property><property name=\"posit"
  "ion\">4</property></packing></child><child><object class=\"GtkCheckButt"
  "on\" id=\"button-more-precision\"><property name=\"label\" translatable"
  "=\"yes\">Show values with more precision</property><property name=\"vis"
  "ible\">True</property><property name=\"can_focus\">True</property><prop"
  "erty name=\"receives_default\">False</property><property name=\"draw_in"
  "dicator\">True</property></object><packing><property name=\"expand\">Fa"
  "lse</property><property name=\"fill\">False</property><property name=\""
  "position\">5</property></packing></child><child><object class=\"GtkBox\""
  " id=\"hbox-refresh-rate\"><property name=\"visible\">True</property><pr"
  "operty name=\"can_focus\">False</property><property name=\"spacing\">6<"
  "/property><child><object class=\"GtkLabel\" id=\"label-refresh-rate\"><"
  "property name=\"visible\">True</property><property name=\"can_focus\">F"
  "alse</property><property name=\"label\" translatable=\"yes\">Refresh ra"
  "te (ms):</property></object><packing><property name=\"expand\">False</p"
  "roperty><property name=\"fill\">False</property><property name=\"positi"
  "on\">0</property></packing></child><child><object class=\"GtkComboBox\""
  " id=\"combobox-refresh-rate\"><property name=\"visible\">True</property"
  "><property name=\"can_focus\">False</property><property name=\"model\">"
  "liststore1</property><child><object class=\"GtkCellRendererText\" id=\""
  "cellrenderertext1\"/><attributes><attribute name=\"text\">0</attribute>"
  "</attributes></child></object><packing><property name=\"expand\">False<"
  "/property><property name=\"fill\">False</property><property name=\"posi"
  "tion\">1</property></packing></child></object><packing><property name=\""
  "expand\">False</property><property name=\"fill\">False</property><prope"
  "rty name=\"position\">6</property></packing></child></object></child><c"
  "hild type=\"label\"><object class=\"GtkLabel\" id=\"label-interface\"><"
  "property name=\"visible\">True</property><property name=\"can_focus\">F"
  "alse</property><property name=\"label\" translatable=\"yes\">&lt;b&gt;I"
  "nterface&lt;/b&gt;</property><property name=\"use_markup\">True</proper"
  "ty></object></child></object><packing><property name=\"expand\">False</"
  "property><property name=\"fill\">False</property><property name=\"posit"
  "ion\">0</property></packing></child><child><object class=\"GtkFrame\" i"
  "d=\"frame-misc\"><property name=\"visible\">True</property><property na"
  "me=\"can_focus\">False</property><property name=\"label_xalign\">0</pro"
  "perty><property name=\"shadow_type\">none</property><child><object clas"
  "s=\"GtkBox\" id=\"vbox-misc\"><property name=\"visible\">True</property"
  "><property name=\"can_focus\">False</property><property name=\"margin_l"
  "eft\">12</property><property name=\"margin_top\">6</property><property "
  "name=\"orientation\">vertical</property><property name=\"spacing\">6</p"
  "roperty><child><object class=\"GtkCheckButton\" id=\"button-prompt-term"
  "inate-task\"><property name=\"label\" translatable=\"yes\">Prompt for t"
  "erminating tasks</property><property name=\"visible\">True</property><p"
  "roperty name=\"can_focus\">True</property><property name=\"receives_def"
  "ault\">False</property><property name=\"draw_indicator\">True</property"
  "></object><packing><property name=\"expand\">False</property><property "
  "name=\"fill\">False</property><property name=\"position\">0</property><"
  "/packing></child><child><object class=\"GtkCheckButton\" id=\"button-sh"
  "ow-status-icon\"><property name=\"label\" translatable=\"yes\">Keep in "
  "the notification area</property><property name=\"visible\">True</proper"
  "ty><property name=\"can_focus\">True</property><property name=\"receive"
  "s_default\">False</property><property name=\"draw_indicator\">True</pro"
  "perty></object><packing><property name=\"expand\">False</property><prop"
  "erty name=\"fill\">False</property><property name=\"position\">1</prope"
  "rty></packing></child></object></child><child type=\"label\"><object cl"
  "ass=\"GtkLabel\" id=\"label-misc\"><property name=\"visible\">True</pro"
  "perty><property name=\"can_focus\">False</property><property name=\"lab"
  "el\" translatable=\"yes\">&lt;b&gt;Miscellaneous&lt;/b&gt;</property><p"
  "roperty name=\"use_markup\">True</property></object></child></object><p"
  "acking><property name=\"expand\">False</property><property name=\"fill\""
  ">False</property><property name=\"position\">1</property></packing></ch"
  "ild></object></child><child type=\"tab\"><object class=\"GtkLabel\"><pr"
  "operty name=\"visible\">True</property><property name=\"can_focus\">Fal"
  "se</property><property name=\"label\" translatable=\"yes\">General</pro"
  "perty></object><packing><property name=\"tab_fill\">False</property></p"
  "acking></child><child><object class=\"GtkBox\"><property name=\"visible"
  "\">True</property><property name=\"can_focus\">False</property><propert"
  "y name=\"border_width\">12</property><property name=\"orientation\">ver"
  "tical</property><child><object class=\"GtkCheckButton\" id=\"pid\"><pro"
  "perty name=\"label\" translatable=\"yes\">PID</property><property name="
  "\"visible\">True</property><property name=\"can_focus\">True</property>"
  "<property name=\"receives_default\">False</property><property name=\"dr"
  "aw_indicator\">True</property></object><packing><property name=\"expand"
  "\">False</property><property name=\"fill\">True</property><property nam"
  "e=\"position\">0</property></packing></child><child><object class=\"Gtk"
  "CheckButton\" id=\"ppid\"><property name=\"label\" translatable=\"yes\""
  ">PPID</property><property name=\"visible\">True</property><property nam"
  "e=\"can_focus\">True</property><property name=\"receives_default\">Fals"
  "e</property><property name=\"draw_indicator\">True</property></object><"
  "packing><property name=\"expand\">False</property><property name=\"fill"
  "\">True</property><property name=\"position\">1</property></packing></c"
  "hild><child><object class=\"GtkCheckButton\" id=\"state\"><property nam"
  "e=\"label\" translatable=\"yes\">State</property><property name=\"visib"
  "le\">True</property><property name=\"can_focus\">True</property><proper"
  "ty name=\"receives_default\">False</property><property name=\"draw_indi"
  "cator\">True</property></object><packing><property name=\"expand\">Fals"
  "e</property><property name=\"fill\">True</property><property name=\"pos"
  "ition\">2</property></packing></child><child><object class=\"GtkCheckBu"
  "tton\" id=\"vbytes\"><property name=\"label\" translatable=\"yes\">Virt"
  "ual Bytes</property><property name=\"visible\">True</property><property"
  " name=\"can_focus\">True</property><property name=\"receives_default\">"
  "False</property><property name=\"draw_indicator\">True</property></obje"
  "ct><packing><property name=\"expand\">False</property><property name=\""
  "fill\">True</property><property name=\"position\">3</property></packing"
  "></child><child><object class=\"GtkCheckButton\" id=\"group-vbytes\"><p"
  "roperty name=\"label\" translatable=\"yes\">Group Virtual Bytes</proper"
  "ty><property name=\"visible\">True</property><property name=\"can_focus"
  "\">True</property><property name=\"receives_default\">False</property><"
  "property name=\"draw_indicator\">True</property></object><packing><prop"
  "erty name=\"expand\">False</property><property name=\"fill\">True</prop"
  "erty><property name=\"position\">4</property></packing></child><child><"
  "object class=\"GtkCheckButton\" id=\"rbytes\"><property name=\"label\" "
  "translatable=\"yes\">Resident Bytes</property><property name=\"visible\""
  ">True</property><property name=\"can_focus\">True</property><property n"
  "ame=\"receives_default\">False</property><property name=\"draw_indicato"
  "r\">True</property></object><packing><property name=\"expand\">False</p"
  "roperty><property name=\"fill\">True</property><property name=\"positio"
  "n\">5</property></packing></child><child><object class=\"GtkCheckButton"
  "\" id=\"group-rbytes\"><property name=\"label\" translatable=\"yes\">Gr"
  "oup Resident Bytes</property><property name=\"visible\">True</property>"
  "<property name=\"can_focus\">True</property><property name=\"receives_d"
  "efault\">False</property><property name=\"draw_indicator\">True</proper"
  "ty></object><packing><property name=\"expand\">False</property><propert"
  "y name=\"fill\">True</property><property name=\"position\">6</property>"
  "</packing></child><child><object class=\"GtkCheckButton\" id=\"uid\"><p"
  "roperty name=\"label\" translatable=\"yes\">UID</property><property nam"
  "e=\"visible\">True</property><property name=\"can_focus\">True</propert"
  "y><property name=\"receives_default\">False</property><property name=\""
  "draw_indicator\">True</property></object><packing><property name=\"expa"
  "nd\">False</property><property name=\"fill\">True</property><property n"
  "ame=\"position\">7</property></packing></child><child><object class=\"G"
  "tkCheckButton\" id=\"cpu\"><property name=\"label\" translatable=\"yes\""
  ">CPU</property><property name=\"visible\">True</property><property name"
  "=\"can_focus\">True</property><property name=\"receives_default\">False"
  "</property><property name=\"draw_indicator\">True</property></object><p"
  "acking><property name=\"expand\">False</property><property name=\"fill\""
  ">True</property><property name=\"position\">8</property></packing></chi"
  "ld><child><object class=\"GtkCheckButton\" id=\"group-cpu\"><property n"
  "ame=\"label\" translatable=\"yes\">Group CPU</property><property name=\""
  "visible\">True</property><property name=\"can_focus\">True</property><p"
  "roperty name=\"receives_default\">False</property><property name=\"draw"
  "_indicator\">True</property></object><packing><property name=\"expand\""
  ">False</property><property name=\"fill\">True</property><property name="
  "\"position\">9</property></packing></child><child><object class=\"GtkCh"
  "eckButton\" id=\"priority\"><property name=\"label\" translatable=\"yes"
  "\">Priority</property><property name=\"visible\">True</property><proper"
  "ty name=\"can_focus\">True</property><property name=\"receives_default\""
  ">False</property><property name=\"draw_indicator\">True</property></obj"
  "ect><packing><property name=\"expand\">False</property><property name=\""
  "fill\">True</property><property name=\"position\">10</property></packin"
  "g></child></object><packing><property name=\"position\">1</property></p"
  "acking></child><child type=\"tab\"><object class=\"GtkLabel\"><property"
  " name=\"visible\">True</property><property name=\"can_focus\">False</pr"
  "operty><property name=\"label\" translatable=\"yes\">Columns</property>"
  "</object><packing><property name=\"position\">1</property><property nam"
  "e=\"tab_fill\">False</property></packing></child><child><placeholder/><"
  "/child><child type=\"tab\"><placeholder/></child></object><packing><pro"
  "perty name=\"expand\">False</property><property name=\"fill\">True</pro"
  "perty><property name=\"position\">0</property></packing></child></objec"
  "t></child><action-widgets><action-widget response=\"-7\">button-close</"
  "action-widget></action-widgets></object></interface>"
};

static const unsigned settings_dialog_ui_length = 16501u;

